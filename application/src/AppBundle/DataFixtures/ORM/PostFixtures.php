<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class PostFixtures
 */
class PostFixtures extends Fixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $post1 = new Post();
        $post1->setTitle('National Enquirer is put up for sale');
        $post1->setDescription("American Media Inc. is looking for a buyer for The National Enquirer.\n
The scandal-hungry tabloid, which has been beset by scandals of its own making in the past year, is on the block, along with American Media's other tabloid magazines.
American Media CEO David Pecker confirmed the plan on Wednesday after the Washington Post reported that the company has come under \"intense pressure\" to part ways with the Enquirer.
The pressure has come from the company's board of directors and from its controlling shareholder Chatham Asset Management, The Post reported. According to the newspaper, Chatham's managing partner Anthony Melchiorre was concerned about both \"the financial difficulties of the tabloid business\" and \"the Enquirer's tactics.\"");
        $post1->setCategory('Business');
        $manager->persist($post1);

        $post2 = new Post();
        $post2->setTitle('Antonio Candreva: Moved by immigrant child left eating tuna and crackers, Inter star makes donation to school');
        $post2->setDescription("Inter Milan footballer Antonio Candreva has offered to make a donation to help an eight-year-old girl from an immigrant family after she was left eating tuna and crackers at her school while other kids tucked into pizza.\n
The episode took place at a primary school in the small village of Minerbe (4,600 people), near Verona, after the girl's parents, from Morocco, fell behind in paying for her lunch fees, the mayor Andrea Girardi told CNN.
\"I spoke this morning with the agent of Antonio Candreva and we agreed that Candreva will make a donation directly to the school,\" said Girardi, who belongs to the anti-immigrant League party.");
        $post2->setCategory('Sport');
        $manager->persist($post2);

        $post3 = new Post();
        $post3->setTitle('Prosecutors are putting the squeeze on Lori Loughlin and wealthy parents');
        $post3->setDescription("On Monday, 13 wealthy parents caught up in the college admissions scam agreed to plead guilty to a conspiracy fraud charge.\n
Less than 24 hours later, 16 parents who did not agree to plead guilty -- actress Lori Loughlin among them -- were hit with another federal charge.
The back-to-back timing of those major announcements shows that prosecutors are using a carrot-and-stick approach to the defendants, legal experts said. Their goal is to pressure them to plead guilty, and fast.");
        $post3->setCategory('World');
        $manager->persist($post3);

        $manager->flush();
    }
}
