<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('POST LIST', $crawler->filter('.page-header')->text());
    }

    public function testNew()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('CREATE NEW POST', $crawler->filter('.page-header')->text());
    }

    public function testView()
    {
        $client = static::createClient();

        $client->request('GET', '/view/0');

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
